import React from 'react';
import skills from '../data/skills';
import Skill from '../components/SkillList';


const Skills = () => {
    return(
        <>
            <h3 className="text-center skill-header">my skills</h3>
            <div className="skills-grid container">
                <Skill back="back-1"
                       skill="skill-1"
                       label="Front-end"
                       data={skills[0]['front-end']}               
                />
                <Skill back="back-2"
                       skill="skill-2"
                       label="Back-end"
                       data={skills[1]['back-end']}               
                />
                <Skill back="back-3"
                       skill="skill-3"
                       label="Other Skills"
                       data={skills[2]['other-skills']}               
                />                
            </div>
                                        
        </>
    )
}

export default Skills;