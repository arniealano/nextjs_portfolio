import React from 'react';
import PortfolioBody from '../components/PortfolioBody';
import PortfolioHeader from '../components/PortfolioHeader';
import PortfolioBackground from '../components/PortfolioBackground.js';

const Portfolio = () => {
    return(
        <>
            <PortfolioHeader />
            <PortfolioBackground>
                <PortfolioBody title='Web Developer Portfolio'
                            details='This project is a simple developer portfolio created using HTML and CSS with bootstrap.'
                            image='/images/capstone-1.png'
                            imageClass='cards-image'
                            link='https://arniealano.gitlab.io/capstone-1/'
                            modal={false}/>
                <hr />
                <PortfolioBody title='Course Booking App'
                            details='An application that allow the users to register, login, create courses, modify courses, delete courses, reactivate courses and enroll to specific courses. This app was created using HTML, CSS, JavaScript as front-end; Mongoose, Express.js, Node.js as back-end; and MongoDB as database.'
                            image='/images/capstone-2.png'
                            imageClass='cards-image'
                            link='https://arniealano.gitlab.io/coursebookingapp/index.html'
                            modal={false}/>
                <hr />     
                <PortfolioBody title='Budget Tracking Application'
                            details='An application that records income and expenses and displays transaction overview and history. It also allows the users to search and filter records and generate their balance trend. This project was created using Javascript Techologies such React.js, Next.js, Express.js and Node.js.'
                            image='/images/capstone-3-main.png'
                            imageClass='cards-image'
                            link='https://students-batch68-arnie-alano-csp3-client.vercel.app/'
                            modal={false}/>   
            </PortfolioBackground>                                            
        </>
    )
}

export default Portfolio;