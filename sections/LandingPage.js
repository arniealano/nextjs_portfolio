import React from 'react';
import NavBar from '../components/NavBar';
import Header from '../components/Header';


const LandingPage = function(){
    return(
        <div className="landing-page" id="home">
            <NavBar />
            <Header />
        </div>
    )
}

export default LandingPage;