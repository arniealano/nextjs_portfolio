import React, {useState} from 'react';
import {Row} from 'react-bootstrap';
import {Col} from 'react-bootstrap';
import {Form} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';


const Contact = () => {
    const [formBody, setFormBody] = useState({
        name: "",
        email: "",
        message: ""
    })
    const {name, email, message} = formBody;
    const onChange = (e) => {
        e.preventDefault();
        setFormBody({
            ...formBody,
            [e.target.name]: e.target.value
        })
    }
    const sendEmail = async () => {
        const res = await fetch(`https://thawing-beyond-50390.herokuapp.com/api/v1/email`,
            {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify(formBody)
            }
        )
        return await res.json();
    }

    const onSubmit = async (e) => {
        e.preventDefault();
        const result = await sendEmail();
        if(result.success === true){
            Swal.fire({
                icon: 'success',
                title: 'Email sent, please wait for our response.'
              })
            setFormBody({
                name: "",
                email: "",
                message: ""
            })
        }
    }
    return(
        <div className="contact-section" id="contact">
            <Row>
                <Col>
                <div className="contact">
                    <div className="contact--form">
                        <h1 className="contact--form--header">Get in touch!</h1>
                        <Form onSubmit={e=>onSubmit(e)}>
                            <Form.Group controlId="fullName">
                                <Form.Control className="border-0 contact-form-inputs" name="name" value={name} type="text" placeholder="Full name" onChange={e=>onChange(e)} required/>
                            </Form.Group>
                            <Form.Group controlId="email">
                                <Form.Control className="border-0 contact-form-inputs" name="email" value={email} type="email" placeholder="Email address" onChange={e=>onChange(e)} required/>
                            </Form.Group>     
                            <Form.Group controlId="message">
                                <Form.Control className="border-0 contact-form-inputs" as="textarea" rows={5} name="message" value={message} placeholder="Enter your message here" onChange={e=>onChange(e)} required />
                            </Form.Group>      
                            <Button type="submit" className="contact-form-button">Send Email</Button>                 
                        </Form>
                        <div className="contact-other pt-5">
                            <a href="https://www.linkedin.com/in/arnie-alano-1045b51b6/" className="contact-other-item text-muted" target="_blank" rel="noreferrer">LinkedIn</a>   
                            <a href="https://gitlab.com/arniealano" className="contact-other-item text-muted" target="_blank" rel="noreferrer">GitLab</a>     
                        </div>  
                    </div>        
                </div>                
                </Col>
            </Row>           
        </div>
    )
}

export default Contact;