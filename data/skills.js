export default [
{
    "front-end" : ["HTML", "CSS", "CSS Grid System", "Bootstrap", "JavaScript", "React.js"]
} ,
{
    "back-end" : ["Mongoose", "Express", "Node.js", "MongoDB", "MySQL", "HANADB"]
},
{
    "other-skills" : ["Git", "Next.js", "SAP ABAP", "UI5/Fiori"]
}  
]