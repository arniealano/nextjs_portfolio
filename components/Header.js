import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Header = function() {
    return(
        <div className="header">
            <Container className="text-center">
                <Row>
                    <Col md={12} lg={10} className="mx-auto">
                        <div className="align-contents">
                            <p className="h1 pt-5 landing-header">Providing Smarter Websites</p>
                            <p className="lead my-4 landing-texts">
                                Hi, I'm Arnie. Specializing in Full Stack Web Development. If you are looking for someone who can build a complete Web Application from scratch, you can get in touch with me <a className="link-text" href="#contact">here</a>.
                            </p>   
                            <a href="#portfolio" className="btn custom-button">View My Projects</a>
                        </div>                 
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Header;