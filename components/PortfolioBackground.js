import React from 'react';

const PortfolioBackground = ({children}) => {
    return(
        <div className="portfolioBackground" id="portfolio">
            {children}
        </div>
    )
}

export default PortfolioBackground;