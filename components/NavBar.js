import React, {useEffect, useState} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Aos from 'aos';
import "aos/dist/aos.css";

const NavBar =  function() {
    let [navClass, setClassName] = useState('pt-4'); //Initialize navClass
    useEffect(() => {
        // Initialize AOS
        Aos.init({
            duration: 500
        }); 
        // Add event
        window.addEventListener("scroll", setClass);
        return () => {
        // Remove event
          window.removeEventListener("scroll", setClass);
        };        

    }, [] );   

    // Set class name..
    const setClass = () =>  (window.scrollY === 0) ? setClassName('pt-4') : setClassName('navOnScroll');
    
    return(
        <Navbar expand="sm" className={`fixed-top custom-nav ${navClass}`} onScroll={setClass}>
            <Container>
                <Navbar.Brand href="#home">
                    <div className="brand-customize">
                        <span className="brand-logo">A</span>
                        <span className="brand-text">RNIE ALANO</span>
                        <span className="brand-subtext">JavaScript Developer</span>
                    </div>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="#navBarCollapse" />
                <Navbar.Collapse id="navBarCollapse">
                    <Nav className="ml-auto text-muted">
                        <Nav.Link href="#home">
                           HOME
                        </Nav.Link>
                        <Nav.Link href="#portfolio">
                           MY PROJECTS
                        </Nav.Link>
                        <Nav.Link href="#contact">
                            CONTACT ME
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )

}

export default NavBar;