import React from 'react';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

const Footer = () => {
    return(
        <>
            <footer className="text-center">
                <div className="format-center-texts">
                    <span className="footer-logo">
                        A
                    </span>
                    <span className="footer-name">ARNIE ALANO</span>
                </div>
                <Row className="footer-contents">
                    <Col md={6}>
                        <ul className="footer-list">
                            <li className="footer-list-item"><a href="#home">HOME</a></li>
                            <li className="footer-list-item"><a href="#portfolio">MY PROJECTS</a></li>
                            <li className="footer-list-item"><a href="#contact">CONTACT ME</a></li>
                        </ul>
                    </Col>
                    <Col md={6}>
                        <div className="copyright">
                            <p>Built by ARNIE ALANO for professional use.</p>
                            <p>Copyright &copy; Arnie Alano 2020.</p>
                            <p>All Rights Reserved.</p>
                        </div>
                    </Col>
                </Row>
            </footer>
        </>
    )
}

export default Footer;