import React from 'react';

const SkillList = ({back, skill, label, data}) => {
    const list = data.map((result, index )=> {
        return(
        <li key={index}>{result}</li>
        )
    })
    return(
        <>
            <div className="skills-item">
                <div className={back}></div>
                <div className={`skills-item--header ${skill}`}>
                    <h3>{label}</h3>
                </div>
                <div className="skills-item--list">
                    <ul>
                        {list}
                    </ul>
                </div>
            </div>        
        </>
    )
}

export default SkillList;