import 'bootstrap/dist/css/bootstrap.min.css';
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import LandingPage from '../sections/LandingPage';
import Portfolio from '../sections/Portfolio';
import Contact from '../sections/Contact';
import Footer from '../components/Footer';
import Skills from '../sections/Skills';

export default function Home() {
  return (
    <>
       <Head><title>Arnie Alano</title></Head>
       <LandingPage />
       <Portfolio />
       <Skills />       
       <Contact />
       <Footer /> 
    </>
  )
}
